import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from 'src/app/model/Employee';
import { EmployeeServiceService } from '../employee-service.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent implements OnInit {
  employees: Employee[] = [];

  constructor(
    public employeeService: EmployeeServiceService,
    public router: Router
  ) {}

  ngOnInit(): void {
    this.fetchEmployee();
  }
  // { pageNumber: 1, PageSize: 10 }
  fetchEmployee() {
    debugger;
    this.employeeService.getEmployee().subscribe((data) => {
      this.employees = data;
      console.log('employee', data);
    });
  }
}
