import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Employee } from 'src/app/model/Employee';
import { PaginatedQuery } from 'src/app/model/PaginationQuery';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class EmployeeServiceService {
  // https://localhost:5001/Employee
  protected url1 = 'https://localhost:5001';
  constructor(private http: HttpClient) {}

  getEmployee(): Observable<any> {
    debugger;
    return this.http.get(`${this.url1}/Employee`);
  }
  // getEmployee2(data: PaginatedQuery) {
  //   return this.http.get<Employee[]>(`${this.url1}/Employee`, {
  //     params: data as any,
  //   });
  // }
}
