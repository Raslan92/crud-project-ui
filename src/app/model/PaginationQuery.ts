export interface PaginatedQuery {
  pageNumber: number;
  PageSize: number;
  keyword?: string;
  sortDirection?: string;
  sortBy?: string;
}
