export class Employee {
  id: number = 0;
  name: string = '';
  email: string = '';
  ImageUrl: any = '';
  password: string = '';
  profilePhoto: string = '';
  currentDeptCode: string = '';
}
